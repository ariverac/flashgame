/* vim:set ts=8 sts=4 sw=4 et:
 *
 * 카드 분배 및 족보 찾기
 */
#ifndef __GAME_H
#define __GAME_H 1

#include "main_proc.h"

#define CARD_SUIT_CLOVER    0x00
#define CARD_SUIT_HEART     0x10
#define CARD_SUIT_DIAMOND   0x20
#define CARD_SUIT_SPADE     0x30
#define CARD_SUIT_M         0x30
#define CARD_NUM_M          0x0f

#define CARD_NUMS_PER_SUIT  13
#define CARD_SUITS_NUM      4 
#define CARD_DEAL_NUM       5 

enum EGameState
{
    GAME_NONE, GAME_INIT, GAME_DIP, GAME_CONNECT,
    GAME_READY, GAME_START, GAME_PROCESS,
    GAME_CONN_WAIT,
    GAME_ENDING, GAME_SUBGAME, GAME_END, GAME_REST
};

enum EPokerCategory
{
    CATEGORY_NONE, CATEGORY_ONE_PAIR, CATEGORY_TWO_PAIR, CATEGORY_HOUSE,
    CATEGORY_STRAIGHT, CATEGORY_BACK_STRAIGHT, CATEGORY_MOUNTAIN,
    CATEGORY_FLUSH, CATEGORY_FULL_HOUSE, CATEGORY_POKER,
    CATEGORY_STRAIGHT_FLUSH, CATEGORY_ROYAL_STRAIGHT_FLUSH,
    CATEGORY_UNKNOWN
};

class CGame : public CMainProc 
{
private:
    typedef CMainProc inherited;

public:
    CGame();
    ~CGame();

private:
    HBITMAP m_hMainBg;

    EGameState m_eGameState;
    EPokerCategory m_ePokerCategory;
    bool m_bGameEndNow; 

    long m_lInputMoney; 
    long m_lBankScore; 
    long m_lWinScore; 
    int m_nSubGameRunPoint;
    int m_nSubGameCnt;
    unsigned int m_nSubGameBankScore;
    DWORD m_next_ms;
    int m_deal_cnt;
    unsigned int m_prevInputAmtActionId;
    unsigned int m_prevDelayReqActionId;

private:
    // card images
    HBITMAP m_hCardSpade[13];
    HBITMAP m_hCardDiamond[13];
    HBITMAP m_hCardHeart[13];
    HBITMAP m_hCardClover[13];
    HBITMAP m_hCardMask;

private:
    struct 
	{
#define N_AUTO_GAME_PERIOD 6000 // default = 6000 msecs
#define N_AUTO_GAME_PERIOD_MIN 1000
        long autoGamePeriod;
#define N_SUBGAME_RUN_POINT 5
        int subGameRunPoint;
    } m_tConfig;

private:
    inline int GetCardDealSuit(int idx)
    {
        return (int) (m_ucDeal[idx] & CARD_SUIT_M);
    }
    inline int GetCardDealNum(int idx)
    {
        return (int) (m_ucDeal[idx] & CARD_NUM_M);
    }
    inline void SetCardDeal(int idx, unsigned char data)
    {
        m_ucDeal[idx] = data;
    }

    bool IsFlush(const int cardSuits[CARD_DEAL_NUM]);
    bool IsStraight(const int cardNums[CARD_DEAL_NUM]);
    EPokerCategory GetStraight(const int cardNums[CARD_DEAL_NUM], bool flush);
    EPokerCategory GetPair(const int cardNums[CARD_DEAL_NUM]);
    static int Compare(const void *a, const void *b);
    EPokerCategory GetCategory(void);
    void SetDeck(void);
    void DealCard(void);

private:
    unsigned char m_ucDeck[CARD_SUITS_NUM * CARD_NUMS_PER_SUIT];
    unsigned char m_ucDeal[CARD_DEAL_NUM];
    bool ProcessGame(DWORD ms);

public:
    virtual void Init(HINSTANCE hInst, HWND hWnd);
    virtual void Begin(void);
    virtual void End(void);
    virtual void Render(HDC hDC = NULL);
    virtual void TimerTick(DWORD ms);
    virtual void Redraw(int redraw);
    virtual void RecvData(GameMsg_t *msg);
    virtual void ReadConfig(void *readhandle);
    virtual int OnKeyPressed(int key);

private:
    BOOL m_automode;
    LONG m_start_ms;
    LONG m_delay_req_ms;
};

#endif /* __POKER_H */
