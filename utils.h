// Some helpers

void nop() {}

bool ExtractDefaultFile(TCHAR *Put, TCHAR *Name, HINSTANCE hy, bool IsMemoryBuffer, TCHAR *Type)
{
	bool E = false;
	HINSTANCE hI = hy;
	HRSRC R;
	if (Type)
		R = FindResource(hI, Name, Type);
	else
		R = FindResource(hI, Name, _T("DATA"));
	if (!R)
		return E;
	HGLOBAL hG = LoadResource(hI, R);
	if (!hG)
		return E;
	int S = SizeofResource(hI, R);
	char *p = (char *)LockResource(hG);
	if (!p)
	{
		FreeResource(R);
		return E;
	}

	// T.6.1+ Allow for returning info in memory
	if (IsMemoryBuffer)
	{
		memcpy(Put, p, S);
		E = true;
	}
	else
	{
		FILE *fp = 0;
		_wfopen_s(&fp,Put, _T("wb"));
		if (fp)
		{
			fwrite(p, 1, S, fp);
			fclose(fp);
			E = true;
		}
	}
	FreeResource(R);
	return E;
}

