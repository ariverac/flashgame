#include <windows.h>
#include <comdef.h>
#include <tchar.h>
#include <string>
using namespace std;


#pragma comment(lib,"crypt32.lib")
#include "activeX.h"

#import "PROGID:ShockwaveFlash.ShockwaveFlash" named_guids exclude("IServiceProvider")
using namespace ShockwaveFlashObjects;

#include "main.h"
#include "game.h"
#include "utils.h"

CLSID iidx = __uuidof(IShockwaveFlash);

DWORD axCookie = 0;
IConnectionPointContainer* cpc = 0;
IConnectionPoint* cp = 0;

HINSTANCE CMain::m_hInstance;
HWND hWnd = nullptr;
CGame *tGameProcAUX = nullptr;

CMain::CMain()
{
	m_hInstance = NULL;
	m_hWnd = NULL;
}

// CMain::~CMain() {{{1
CMain::~CMain()
{
}

// Our advisor
class FlashNotify : public _IShockwaveFlashEvents
{
	private:
		int ref;
	public:
		FlashNotify()
		{
			ref = 1;
		}

		// IUnknown
		ULONG __stdcall AddRef()
		{
			return ++ref;
		}

		ULONG __stdcall Release()
		{
			return --ref;
		}

		HRESULT __stdcall QueryInterface(const IID& id,void**p)
		{
			if (id == IID_IUnknown || id == __uuidof(_IShockwaveFlashEvents) || id == IID_IDispatch)
			{
				*p = this;
				AddRef();
				return S_OK;
			}
			return E_NOINTERFACE;
		}

		// IDispatch
		HRESULT _stdcall GetTypeInfoCount(unsigned int * c) 
		{
			if (!c)
				return E_POINTER;
			*c = 0;
			return S_OK;
		}

		HRESULT __stdcall GetTypeInfo(unsigned int,		LCID ,		ITypeInfo FAR* FAR*)
		{
			return E_NOTIMPL;
		}
		HRESULT _stdcall GetIDsOfNames(REFIID,LPOLESTR*,unsigned int,LCID,DISPID FAR*) 
		{
			return E_NOTIMPL;
		}

		HRESULT _stdcall Invoke(DISPID d,REFIID ,LCID ,WORD ,DISPPARAMS* p,VARIANT FAR*,EXCEPINFO FAR* ,unsigned int FAR*)
		{
			if (d != 0xC5) // DISPID for "Flash Call"
				return S_OK;
			if (!p)
				return E_POINTER;
			if (p->cArgs != 1)
				return E_INVALIDARG;
			if (p->rgvarg[0].vt != VT_BSTR)
				return E_INVALIDARG;

			//wstring w = L"You clicked a flash button!\r\nYou receive:\r\n\r\n";
			wstring w = L"";
			w += p->rgvarg[0].bstrVal;
			tGameProcAUX->OnKeyPressed(VK_SPACE);
			//MessageBox(0,w.c_str(),L"Result",MB_ICONINFORMATION);

			return S_OK;
		}
};

FlashNotify fn;

void CMain::functionflash(HWND hX, const char* StrArg)
{
	char Strargument[512];
	char* StrArg1 = "<invoke name=\"call_swf_function\" returntype=\"xml\"><arguments><string>";
	char* StrArg2 = "</string></arguments></invoke>";
	//StrArg = "<invoke name=\"call_swf_function\" returntype=\"xml\"><arguments><string>Ael</string></arguments></invoke>";
	
	sprintf_s(Strargument, sizeof(Strargument), "%s%s%s", StrArg1, StrArg, StrArg2);
	IShockwaveFlash* p = 0;
	HRESULT hr = (HRESULT)SendMessage(hX, AX_QUERYINTERFACE, (WPARAM)&iidx, (LPARAM)&p);
	if (p && SUCCEEDED(hr))
	{
		// Sample: Call a function
		try
		{
			//_bstr_t fu(L"<invoke name=\"call_swf_function\" returntype=\"xml\"><arguments><string>Ael</string></arguments></invoke>");
			//_bstr_t fu(StrArg);
			_bstr_t fu(Strargument);
			_bstr_t rs = p->CallFunction(fu);
			nop();
		}
		catch (...)
		{
			nop();
		}
		p->Release();
	}
};

INT_PTR CALLBACK CMain::WndProc(HWND hh,UINT mm,WPARAM ww,LPARAM lParam)
{
	int r;
	HWND hX = GetDlgItem(hh,888);
	PAINTSTRUCT ps;
	CMainProc *lpGameProc = (CMainProc *)GetWindowLongPtrA(hWnd, 0);

	switch(mm) 
	{
		case WM_COMMAND:
		{
			if (LOWORD(ww) == 101)
			{				
				for (int i = 0; i < tGameProcAUX->GetLogRowSize(); i++)
				{
					const char *szLog = tGameProcAUX->GetLogString(i);
					if (szLog == NULL)
						break;
					CMain::functionflash(hX, (LPCSTR)szLog);					
				}				
			}
			return 0;
		}
		case WM_CREATE:
			MessageBoxA(hh, "WM_CREATE", "Result", MB_ICONINFORMATION);			
			return 0;
		case WM_INITDIALOG:
		{						
			TCHAR dx[1000] = {0};
			GetTempPath(1000,dx);
			TCHAR df[1000] = {0};;
			GetTempFileName(dx,L"swf",0,df);
			DeleteFile(df);
			wcscat_s(df,1000,L".swf");
			DeleteFile(df);
			ExtractDefaultFile(df,L"flash",0,0,L"data");

			SendMessage(hX,AX_INPLACE,1,0);
			SendMessage(hh,WM_SIZE,0,0);

			// Set the object
			IShockwaveFlash* p = 0;
			CLSID iidx = __uuidof(IShockwaveFlash);
			HRESULT hr = (HRESULT)SendMessage(hX,AX_QUERYINTERFACE,(WPARAM)&iidx,(LPARAM)&p);
			if (p)
			{
				_bstr_t x(df);
				hr = p->put_BackgroundColor(RGB(0,255,255));
				hr = p->put_Movie(x);
				// Notification
				AX* iax = (AX*)SendMessage(hX,AX_GETAXINTERFACE,0,0);
				if (iax)
				{
					axCookie = AXConnectObject(iax->OleObject,__uuidof(_IShockwaveFlashEvents),(IUnknown*)&fn,&cpc,&cp);
				}
				p->Release();
			}
			break;
		}		
		case WM_SIZE:
		{
			RECT rc;
			GetClientRect(hh,&rc);
			SetWindowPos(hX,0,0,0,rc.right,rc.bottom,SWP_SHOWWINDOW);
			return 0;
		}		
		case WM_PAINT:		
			BeginPaint(hh, &ps);	
			//MessageBoxA(hh, "WM_PAINT", "Result", MB_ICONINFORMATION);						
			EndPaint(hh, &ps);
			break;		
		
		case WM_CLOSE:
		{
			EndDialog(hh,0);
			return 0;
		}
		case WM_KEYDOWN:
			if ((lParam & (0x1 << 30)) == 0x00)
			{			
				if (lpGameProc)
				{
					r = lpGameProc->OnKeyPressed(ww);
				}
			}						
			MessageBoxA(hh, "WM_KEYDOWN", "Result", MB_ICONINFORMATION);
			break;
		case WM_LBUTTONDBLCLK:
			MessageBoxA(hh, "WM_LBUTTONDBLCLK", "Result", MB_ICONINFORMATION);
			break;
	}
	return 0;
}

// CMain::RegisterClass() {{{1
ATOM CMain::RegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = (WNDPROC)WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = sizeof(void *); // CGamePtr 포인터를 저장하기 위해.
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, (LPCTSTR)IDI_GAMEDEMO);
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = (LPCWSTR)IDC_GAMEDEMO;
	wcex.lpszClassName = m_szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);

	return RegisterClassEx(&wcex);
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	CMain game;
	return game.Main(hInstance, nCmdShow);
}

int CMain::Main(HINSTANCE hInstance, int nCmdShow)
{
	m_hInstance = hInstance;

	OleInitialize(0);
	//	CoInitializeEx(0,COINIT_APARTMENTTHREADED;
	AXRegister();
	//RegisterClass(hInstance);
	if (!InitInstance(hInstance, nCmdShow))
		return false;
	return 0;
}

bool CMain::InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	tGameProcAUX = &(CMain::m_tGameProc);
	CMain::m_tGameProc.Init(hInstance, 0);
	SetWindowLongPtr(0, 0, (LONG)&m_tGameProc);    
	CMain::m_tGameProc.Begin();
	DialogBox(hInstance, L"DIALOG_MAIN", hWnd, WndProc);	
	
	return true;
}
