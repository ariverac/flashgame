
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "game.h"
#include "../src/comm_proc.h"


#define random(n)  (rand() % (n))
#define randomize() srand((unsigned)time(NULL)) 

#define CARD_NUM_ACE     0
#define CARD_NUM_10      9
#define CARD_NUM_JACK   10
#define CARD_NUM_QUEEN  11
#define CARD_NUM_KING   12

static const long s_WinScores[] = 
{
    0, 100, 300, 800, 1500, 1500, 2000, 3000, 5000,
    10000, 20000, 30000
};

CGame::CGame()
{
    m_tConfig.autoGamePeriod = N_AUTO_GAME_PERIOD;
    m_tConfig.subGameRunPoint = N_SUBGAME_RUN_POINT;

    m_lWinScore = 0;
    m_lBankScore = 0;
    m_lInputMoney = 100000;
    m_nSubGameRunPoint = 0;
    m_nSubGameBankScore = 0;
    m_automode = FALSE;
    m_eGameState = GAME_NONE;
    m_ePokerCategory = CATEGORY_UNKNOWN;
    m_prevInputAmtActionId = (unsigned int) -1;
    m_prevDelayReqActionId = (unsigned int) -1;

    SetDeck();
}

CGame::~CGame()
{
}

bool CGame::IsFlush(const int cardSuits[CARD_DEAL_NUM])
{
    if (   cardSuits[0] == cardSuits[1]
           && cardSuits[0] == cardSuits[2]
           && cardSuits[0] == cardSuits[3]
           && cardSuits[0] == cardSuits[4])
    {
        return true;
    }

    return false;
}

bool CGame::IsStraight(const int cardNums[CARD_DEAL_NUM])
{
    if (   (cardNums[1] + 1) == cardNums[2]
           && (cardNums[1] + 2) == cardNums[3]
           && (cardNums[1] + 3) == cardNums[4])
    {
        if ((cardNums[0] == CARD_NUM_ACE && cardNums[1] == CARD_NUM_10)
            || (cardNums[0] + 1 == cardNums[1]))
        {
            return true;
        }
    }

    return false;
}

EPokerCategory CGame::GetStraight(const int cardNums[CARD_DEAL_NUM], bool flush)
{
    EPokerCategory category = CATEGORY_NONE;

    if (IsStraight(cardNums))
    {
        if (flush)
        {
            if (cardNums[0] == CARD_NUM_ACE && cardNums[1] == CARD_NUM_10)
                category = CATEGORY_ROYAL_STRAIGHT_FLUSH;
            else
                category = CATEGORY_STRAIGHT_FLUSH;
        }
        else if (cardNums[0] == CARD_NUM_ACE)
        {
            if (cardNums[1] == CARD_NUM_10)
                category = CATEGORY_MOUNTAIN;
            else
                category = CATEGORY_BACK_STRAIGHT;
        }
        else
            category = CATEGORY_STRAIGHT;
    }

    return category;
}

EPokerCategory CGame::GetPair(const int cardNums[CARD_DEAL_NUM])
{
    int i, cnt, pairCnt, maxCnt;
    EPokerCategory category = CATEGORY_NONE;

    cnt = maxCnt = 1;
    pairCnt = 0;
    for (i = 0; i < CARD_DEAL_NUM; ++i)
    {
        if (i < CARD_DEAL_NUM - 1 && cardNums[i] == cardNums[i + 1])
        {
            if (++cnt > maxCnt)
                maxCnt = cnt;
            continue;
        }
        if (cnt > 1)
            ++pairCnt;
        cnt = 1;
    }

    if (maxCnt > 3)
        category = CATEGORY_POKER;
    else if (maxCnt == 3)
    {
        if (pairCnt > 1)
            category = CATEGORY_FULL_HOUSE;
        else
            category = CATEGORY_HOUSE;
    }
    else if (pairCnt > 1)
        category = CATEGORY_TWO_PAIR;
    else if (pairCnt)
        category = CATEGORY_ONE_PAIR;

    return category;
}

/* static */ 
int CGame::Compare(const void *a, const void *b)
{
    return ((*(int*)a) - (*(int*)b));
}

EPokerCategory CGame::GetCategory(void)
{
    EPokerCategory category;
    int i;
    bool flush;
    int cardNums[CARD_DEAL_NUM];
    int cardSuits[CARD_DEAL_NUM];

    for (i = 0; i < CARD_DEAL_NUM; ++i)
    {
        cardSuits[i] = GetCardDealSuit(i);
        cardNums[i] = GetCardDealNum(i);
    }
    qsort(cardNums, CARD_DEAL_NUM, sizeof(int), Compare);

    flush = IsFlush(cardSuits);

    category = GetStraight(cardNums, flush);
    if (category == CATEGORY_NONE)
        category = GetPair(cardNums);

    if (flush && category < CATEGORY_FLUSH)
        category = CATEGORY_FLUSH;

    return category;
}

void CGame::DealCard(void)
{
    int i, j;

    for (i = 0; i < CARD_DEAL_NUM; )
    {
        unsigned char b = m_ucDeck[random(52)];
        for (j = 0; j < i; ++j)
            if (b == m_ucDeal[j])
                break;
        if (j == i)
            m_ucDeal[i++] = b;
    }
}

void CGame::SetDeck(void)
{
    int i, base;

    randomize();

    base = 0;
    for (i = 0; i < CARD_NUMS_PER_SUIT; ++i)
        m_ucDeck[base + i] = i | CARD_SUIT_SPADE;

    base += CARD_NUMS_PER_SUIT;
    for (i = 0; i < CARD_NUMS_PER_SUIT; ++i)
        m_ucDeck[base + i] = i | CARD_SUIT_DIAMOND;

    base += CARD_NUMS_PER_SUIT;
    for (i = 0; i < CARD_NUMS_PER_SUIT; ++i)
        m_ucDeck[base + i] = i | CARD_SUIT_HEART;

    base += CARD_NUMS_PER_SUIT;
    for (i = 0; i < CARD_NUMS_PER_SUIT; ++i)
        m_ucDeck[base + i] = i | CARD_SUIT_CLOVER;

    DealCard();
}

void CGame::Init(HINSTANCE hInst, HWND hWnd)
{
    inherited::Init(hInst, hWnd);

	inherited::Log(1, "*****************************************");
	inherited::Log(1, "*      게임 데모 v%u.%u.%u (%s)     *",
        GAME_DEMO_VER_MAJ, GAME_DEMO_VER_MIN, GAME_DEMO_VER_PATCH, __DATE__);
	inherited::Log(1, "****************HEY ARIEL*************************");
	/*    char szPath[MAX_PATH];
    char szFilename[MAX_PATH];

    if (inherited::GetConfigFullPath("Image", szPath, sizeof(szPath), true) != 0)
        strcpy_s(szPath, sizeof(szPath), ".\\Image");

    sprintf_s(szFilename, sizeof(szFilename), "%s\\poker_bg.bmp", szPath);
    m_hMainBg = (HBITMAP) LoadImage(m_hInstance, szFilename, IMAGE_BITMAP,
                                    0, 0,
                                    (LR_LOADFROMFILE | LR_CREATEDIBSECTION
                                     | LR_DEFAULTSIZE));

    for (int i = 0; i < 13; i++)
    {
        sprintf_s(szFilename, sizeof(szFilename), "%s\\Card1\\%03d.bmp",
                  szPath, i + 1);
        m_hCardSpade[i] = (HBITMAP) LoadImage(m_hInstance, szFilename,
                                              IMAGE_BITMAP, 0, 0,
                                              (LR_LOADFROMFILE
                                               | LR_CREATEDIBSECTION
                                               | LR_DEFAULTSIZE));
    }

    for (int i = 0; i < 13; i++)
    {
        sprintf_s(szFilename, sizeof(szFilename), "%s\\Card2\\%03d.bmp",
                  szPath, i + 1);
        m_hCardDiamond[i] = (HBITMAP) LoadImage(m_hInstance, szFilename,
                                                IMAGE_BITMAP, 0, 0,
                                                (LR_LOADFROMFILE
                                                 | LR_CREATEDIBSECTION
                                                 | LR_DEFAULTSIZE));
    }

    for (int i = 0; i < 13; i++)
    {
        sprintf_s(szFilename, sizeof(szFilename), "%s\\Card3\\%03d.bmp",
                  szPath, i + 1);
        m_hCardHeart[i] = (HBITMAP) LoadImage(m_hInstance, szFilename,
                                              IMAGE_BITMAP, 0, 0,
                                              (LR_LOADFROMFILE
                                               | LR_CREATEDIBSECTION
                                               | LR_DEFAULTSIZE));
    }

    for (int i = 0; i < 13; i++)
    {
        sprintf_s(szFilename, sizeof(szFilename), "%s\\Card4\\%03d.bmp",
                  szPath, i + 1);
        m_hCardClover[i] = (HBITMAP) LoadImage(m_hInstance, szFilename,
                                               IMAGE_BITMAP, 0, 0,
                                               (LR_LOADFROMFILE
                                                | LR_CREATEDIBSECTION
                                                | LR_DEFAULTSIZE));
    }

    sprintf_s(szFilename, sizeof(szFilename), "%s\\card_mask.bmp", szPath);
    m_hCardMask = (HBITMAP) LoadImage(m_hInstance, szFilename,
                                      IMAGE_BITMAP, 0, 0,
                                      (LR_LOADFROMFILE | LR_CREATEDIBSECTION
                                       | LR_DEFAULTSIZE));
	*/
}

void CGame::Begin()
{
    inherited::Begin();

    DealCard();
    m_ePokerCategory = GetCategory();
}

void CGame::End(void)
{
    int i = 0;

    for (i = 0; i < 13; i++)
        DeleteObject(m_hCardSpade[i]);
    for (i = 0; i < 13; i++)
        DeleteObject(m_hCardDiamond[i]);
    for (i = 0; i < 13; i++)
        DeleteObject(m_hCardHeart[i]);
    for (i = 0; i < 13; i++)
        DeleteObject(m_hCardClover[i]);

    inherited::End();
}

void CGame::Render(HDC hDC_0)
{
    inherited::Render(hDC_0);

    HDC hDC, memDC, backBuff;
    HFONT font;
    PAINTSTRUCT ps;
    char szBuf[512];
    int len, size;
    RECT rect;
    HBITMAP bitmap;

    if (hDC_0 == NULL)
        hDC = GetDC(m_hWnd);
    else
        hDC = hDC_0;

    memDC = CreateCompatibleDC(hDC);
    GetClientRect(m_hWnd, &rect);
    bitmap = CreateCompatibleBitmap(hDC, rect.right, rect.bottom);
    backBuff = CreateCompatibleDC(hDC);
    SelectObject(memDC, bitmap);
    Rectangle(memDC, 0, 0, rect.right, rect.bottom);

    SelectObject(backBuff, m_hMainBg);
    BitBlt(memDC, 0, 0, rect.right, rect.bottom, backBuff, 0, 0, SRCCOPY);

    int i, px, py;
    px = 205;
    py = 198;
    for (i = 0; i < CARD_DEAL_NUM; i++)
    {
        int x = px + (i * 110);
        int cardNum = GetCardDealNum(i);
        int cardSuit = GetCardDealSuit(i);

        SelectObject(backBuff, m_hCardMask);
        BitBlt(memDC, x, py, 100, 150, backBuff, 0, 0, SRCAND);

        switch (cardSuit)
        {
            case CARD_SUIT_SPADE:
                SelectObject(backBuff, m_hCardSpade[cardNum]);
                break;
            case CARD_SUIT_DIAMOND:
                SelectObject(backBuff, m_hCardDiamond[cardNum]);
                break;
            case CARD_SUIT_HEART:
                SelectObject(backBuff, m_hCardHeart[cardNum]);
                break;
            case CARD_SUIT_CLOVER:
                SelectObject(backBuff, m_hCardClover[cardNum]);
                break;
        }

        BitBlt(memDC, x, py, 100, 150, backBuff, 0, 0, SRCPAINT);
    }

    RECT rt;

    size = 28;
    sprintf_s(szBuf, sizeof(szBuf), "청소년불가 게임 데모 v%u.%u.%u",
              GAME_DEMO_VER_MAJ, GAME_DEMO_VER_MIN, GAME_DEMO_VER_PATCH);
    len = strlen(szBuf);

    rt.top = 25;
    rt.bottom = rt.top + size;
    rt.left = (rect.right / 2) - (len * size / 4);
    rt.right = rt.left + len * size;
    font = CreateFont(size, 0, 0, 0, FW_BOLD, 0, 0, 0, DEFAULT_CHARSET,
                      0, 0, CLEARTYPE_QUALITY, 0, NULL);
    SelectObject(memDC, font);
    SetTextColor(memDC, RGB(255, 255, 255));
    SetBkColor(memDC, RGB(0, 0, 0));
    SetBkMode(memDC, TRANSPARENT);

    DrawText(memDC, (LPCWSTR)szBuf, len, &rt, DT_LEFT);
    DeleteObject(font);

    rt.top = 460;
    rt.left = 405;
    rt.bottom = rect.bottom;
    rt.right = rt.left + 250;

    font = CreateFont(14, 0, 0, 0, 0, 0, 0, 0, DEFAULT_CHARSET,
                      0, 0, CLEARTYPE_QUALITY, 0, NULL);
    SelectObject(memDC, font);
    SetTextColor(memDC, RGB(255, 255, 255));
    SetBkColor(memDC, RGB(0, 0, 0));
    SetBkMode(memDC, TRANSPARENT);

    sprintf_s(szBuf, sizeof(szBuf),
              " 원 페어                %6ld\n"
              " 투 페어                %6ld\n"
              " 하우스                 %6ld\n"
              " 스트레이트             %6ld\n"
              " 백 스트레이트          %6ld\n"
              " 마운틴                 %6ld\n"
              " 플러쉬                 %6ld\n"
              " 풀 하우스              %6ld\n"
              " 포커                   %6ld\n"
              " 스트레이트 플러쉬      %6ld\n"
              " 로얄 이스탄불          %6ld",
              s_WinScores[1], s_WinScores[2], s_WinScores[3],
              s_WinScores[4], s_WinScores[5], s_WinScores[6],
              s_WinScores[7], s_WinScores[8], s_WinScores[9],
              s_WinScores[10], s_WinScores[11]);
    DrawText(memDC, (LPCWSTR)szBuf, strlen(szBuf), &rt, DT_LEFT);
    DeleteObject(font);

    rt.top = 460;
    rt.left = 685;
    rt.bottom = rect.bottom;
    rt.right = rect.right;
    font = CreateFont(20, 0, 0, 0, FW_BOLD, 0, 0, 0, DEFAULT_CHARSET,
                      0, 0, CLEARTYPE_QUALITY, 0, NULL);
    SelectObject(memDC, font);
    SetTextColor(memDC, RGB(255, 255, 255));
    SetBkColor(memDC, RGB(0, 0, 0));
    SetBkMode(memDC, TRANSPARENT);

    sprintf_s(szBuf, sizeof(szBuf), " 이용 금액 : %7ld ", m_lInputMoney);
    DrawText(memDC, (LPCWSTR)szBuf, strlen(szBuf), &rt, DT_LEFT);

    rt.top += 30;
    sprintf_s(szBuf, sizeof(szBuf), " 배팅 금액 : %7ld ", 100L);
    DrawText(memDC, (LPCWSTR)szBuf, strlen(szBuf), &rt, DT_LEFT);

    rt.top += 30;
    sprintf_s(szBuf, sizeof(szBuf), " 당첨 점수 : %7ld ", m_lWinScore);
    DrawText(memDC, (LPCWSTR)szBuf, strlen(szBuf), &rt, DT_LEFT);

    rt.top += 30;
    sprintf_s(szBuf, sizeof(szBuf), " 누적 점수 : %7ld ", m_lBankScore);
    DrawText(memDC, (LPCWSTR)szBuf, strlen(szBuf), &rt, DT_LEFT);
    DeleteObject(font);

    switch (m_ePokerCategory)
    {
        case CATEGORY_NONE:
            sprintf_s(szBuf, sizeof(szBuf), "족보 없음");
            break;
        case CATEGORY_ONE_PAIR:
            sprintf_s(szBuf, sizeof(szBuf), "원 페어");
            break;
        case CATEGORY_TWO_PAIR:
            sprintf_s(szBuf, sizeof(szBuf), "투 페어");
            break;
        case CATEGORY_HOUSE:
            sprintf_s(szBuf, sizeof(szBuf), "하우스");
            break;
        case CATEGORY_STRAIGHT:
            sprintf_s(szBuf, sizeof(szBuf), "스트레이트");
            break;
        case CATEGORY_BACK_STRAIGHT:
            sprintf_s(szBuf, sizeof(szBuf), "백 스트레이트");
            break;
        case CATEGORY_MOUNTAIN:
            sprintf_s(szBuf, sizeof(szBuf), "마운틴");
            break;
        case CATEGORY_FLUSH:
            sprintf_s(szBuf, sizeof(szBuf), "플러쉬");
            break;
        case CATEGORY_FULL_HOUSE:
            sprintf_s(szBuf, sizeof(szBuf), "풀 하우스");
            break;
        case CATEGORY_POKER:
            sprintf_s(szBuf, sizeof(szBuf), "포커");
            break;
        case CATEGORY_STRAIGHT_FLUSH:
            sprintf_s(szBuf, sizeof(szBuf), "스트레이트 플러쉬");
            break;
        case CATEGORY_ROYAL_STRAIGHT_FLUSH:
            sprintf_s(szBuf, sizeof(szBuf), "로얄 이스탄불");
            break;
        default:
            szBuf[0] = '\0';
            break;
    }

    size = 20;
    font = CreateFont(size, 0, 0, 0, FW_BOLD, 0, 0, 0, DEFAULT_CHARSET,
                      0, 0, CLEARTYPE_QUALITY, 0, NULL);
    SelectObject(memDC, font);
    SetTextColor(memDC, RGB(255, 255, 255));
    SetBkColor(memDC, RGB(0, 0, 0));
    SetBkMode(memDC, TRANSPARENT);

    rt.top = 162;
    rt.left = 200;
    rt.bottom = rt.top + size;
    rt.right = rect.right - 185;
    DrawText(memDC, (LPCWSTR)szBuf, strlen(szBuf), &rt, DT_RIGHT);
    DeleteObject(font);

    font = CreateFont(12, 0, 0, 0, 0, 0, 0, 0, DEFAULT_CHARSET,
                      0, 0, CLEARTYPE_QUALITY, 0, NULL);
    SelectObject(memDC, font);
    SetTextColor(memDC, RGB(255, 255, 255));
    SetBkColor(memDC, RGB(0, 0, 0));
    SetBkMode(memDC, TRANSPARENT);

    rt.top = 445;
    rt.left = 10;
    rt.bottom = rect.bottom;
    rt.right = rt.left + 330;
    for (i = 0; i < GetLogRowSize(); i++)
    {
        const char *szLog = GetLogString(i);
        if (szLog == NULL)
            break;
        DrawText(memDC, (LPCWSTR)szLog, strlen(szLog), &rt, DT_LEFT);
        rt.top += 16;
    }
    DeleteObject(font);

    // Error/Event message
    if (m_eError != 0 || m_eGameState == GAME_CONN_WAIT)
    {
        size = 60;
        font = CreateFont(size, 0, 0, 0, 0, 0, 0, 0, DEFAULT_CHARSET,
                          0, 0, CLEARTYPE_QUALITY, 0, NULL);
        SelectObject(memDC, font);
        SetTextColor(memDC, RGB(128, 0, 0));
        SetBkColor(memDC, RGB(255, 255, 255));
        SetBkMode(memDC, OPAQUE); // no transparent

        rt.top = 180;
        rt.left = 0;
        rt.bottom = rect.bottom;
        rt.right = rect.right;

        // 빈 줄
        len = ErrorMsgFormat(szBuf, sizeof(szBuf), "  ");
        DrawText(memDC, (LPCWSTR)szBuf, len, &rt, DT_CENTER);

        rt.top += size;
        if (m_eError != 0)
        {
            len = GetErrorMessageStr(m_eError, szBuf, sizeof(szBuf));
            DrawText(memDC, (LPCWSTR)szBuf, len, &rt, DT_CENTER);

            rt.top += size;
            len = ErrorMsgFormat(szBuf, sizeof(szBuf),
                                 "카운터에 문의하세요.");
            DrawText(memDC, (LPCWSTR)szBuf, len, &rt, DT_CENTER);
        }
        else if (m_eGameState == GAME_CONN_WAIT)
        {
            char szTmpBuf[256];

            sprintf_s(szTmpBuf, sizeof(szTmpBuf), "접속 시도중 %d...",
                      m_nErrorFixTry);
            len = ErrorMsgFormat(szBuf, sizeof(szBuf), szTmpBuf);
            DrawText(memDC, (LPCWSTR)szBuf, strlen(szBuf), &rt, DT_CENTER);
        }

        // 빈 줄
        rt.top += size;
        len = ErrorMsgFormat(szBuf, sizeof(szBuf), "  ");
        DrawText(memDC, (LPCWSTR)szBuf, len, &rt, DT_CENTER);

        DeleteObject(font);
    }

    BitBlt(hDC, 0, 0, rect.right, rect.bottom, memDC, 0, 0, SRCCOPY);

    DeleteDC(memDC);
    DeleteObject(bitmap);
    DeleteDC(backBuff);
    if (hDC_0 == NULL)
        ReleaseDC(m_hWnd, hDC);
}

int CGame::OnKeyPressed(int key)
{
    int r;
    
    if ((r = inherited::OnKeyPressed(key)) >= 0)
        return r;

    if (IsCommConnected() == false)
        return r;

    r = 0;
    switch (key)
    {
        case VK_SPACE:
            if (IsCommReady() == false)
                break;
            if (m_eGameState == GAME_READY)
            {
                Log(1, "게임 시작");
                m_eGameState = GAME_START;
                m_ePokerCategory = CATEGORY_UNKNOWN;
                m_lWinScore = 0;
            }
            else if (m_eGameState == GAME_PROCESS)
            {
                m_bGameEndNow = true;
            }
            break;

        case VK_F7:
            if (m_automode == false)
            {
                m_automode = true;
                Log(1, "자동 게임 모드 시작");
            }
            break;

        case VK_F8:
            if (m_automode)
            {
                m_automode = false;
                Log(1, "자동 게임 모드 종료");
            }
            break;

        default:
            r = -1;
            break;
    }

    return r;
}

bool CGame::ProcessGame(DWORD ms)
{
    unsigned int winScore;
    bool r = true; // default = redraw
    const int betAmt = 100;
    const int cardDealPeriod = 200;

    if (m_eError)
        return false;

    switch (m_eGameState)
    {
        case GAME_NONE:
            m_eGameState = GAME_INIT;
            randomize();
            break;

        case GAME_INIT:
            if (IsCommConnected())
            {
                m_eGameState = GAME_READY;
            }
            else
            {
                m_nErrorFixTry = 1;
                m_next_ms = ms + 1000;
                m_eGameState = GAME_CONN_WAIT;
                Log(1, "OIDD(표시장치) 접속 시도중...");
            }
            break;

        case GAME_CONN_WAIT:
            if (ms >= m_next_ms)
            {
                if (++m_nErrorFixTry == 10)
                {
                    m_eError = D_USER_ERROR_NOCONNECT;
                    return true;
                }
                m_next_ms = ms + 1000;
            }

            if (IsCommConnected())
            {
                m_eGameState = GAME_READY;
                m_eError = 0;
            }
            break;

        case GAME_READY:
            if (m_automode)
                OnKeyPressed(VK_SPACE);
            m_bGameEndNow = false;
            r = false;
            break;

        case GAME_START:
            if ((r = IsCommReady()) == false)
                break;

            if (m_lBankScore <= 0 && m_lInputMoney <= 0)
            {
                // OIDD에서 투입금액을 보내지 않아도 POT money가 있을 수
                // 있는 데, 어느 게임기의 경우 전체 reset해도 게임 money는
                // reboot후에도 보존된다.  그런 경우 OIDD에서는 투입금액은
                // 0이지만 POT money는 게임기 reset 전의 금액이 된다.
                // 돈이 떨어졌을 때, 게임기에서 강제로 금액을 채우면 그런
                // 경우에 대해 동일한 결과가 된다.
                m_lInputMoney = 50000;
#if 0
                m_eGameState = GAME_READY;
                Log(1, TEXT("사용 할수 있는 포인트가 없습니다."));
                OnKeyPressed(VK_F8);
                return;
#endif
            }

            MsgSend(ASN_MSG_TYPE1_DELIVER, ASN_MSG_TYPE2_D_GAME_START, 0, 0, TRUE);
            LinkSendPeriodSet(LINK_SEND_PERIOD_GAME);
            m_start_ms = ms; // save start time

            if (m_lBankScore > 0)
                m_lBankScore -= betAmt;
            else
                m_lInputMoney -= betAmt;

            MsgSend(ASN_MSG_TYPE1_DELIVER, ASN_MSG_TYPE2_D_BET_AMT,betAmt, 0, TRUE);
            MsgSend(ASN_MSG_TYPE1_DELIVER, ASN_MSG_TYPE2_D_POT_AMT,m_lInputMoney, 0, TRUE);

            m_eGameState = GAME_PROCESS;
            m_next_ms = 0;
            m_deal_cnt = 0;
            break;

        case GAME_PROCESS:
            // 이전 게임 시작 메시지 처리가 정상적으로 끝날 때까지는 아무
            // 동작도 수행하지 않음.
            if ((r = IsCommReady()) == false)
                break;

            if (m_bGameEndNow || ms >= m_next_ms)
            {
                DealCard();
                m_next_ms = ms + cardDealPeriod;

                // Game Period가 긴 경우 최종 족보 확인 시간을 2초 정도 보장.
                int cardDealCnt = 0;
                if (m_tConfig.autoGamePeriod > 2000)
                {
                    cardDealCnt = (m_tConfig.autoGamePeriod - 2000);
                    cardDealCnt /= cardDealPeriod;
                }
                if (cardDealCnt < 3)
                    cardDealCnt = 3;
                if (++m_deal_cnt > cardDealCnt && IsCommReady())
                {
                    m_eGameState = GAME_ENDING;
                }
            }
            break;

        case GAME_ENDING:
            if ((r = IsCommReady()) == false)
                break;

            m_ePokerCategory = GetCategory();
            if ((unsigned int) m_ePokerCategory < ARYSIZ(s_WinScores))
                m_lWinScore = s_WinScores[(int) m_ePokerCategory];
            else
                m_lWinScore = 0;

            m_lBankScore += m_lWinScore;

            MsgSend(ASN_MSG_TYPE1_DELIVER, ASN_MSG_TYPE2_D_WIN_SCORE,
                    m_lWinScore, 0, TRUE);

            /*
             * 부가게임
             */
            if (m_lWinScore)
            {
                if (m_nSubGameRunPoint > 0)
                    --m_nSubGameRunPoint;
            }
            else
            {
                ++m_nSubGameRunPoint;
            }
            if (m_nSubGameRunPoint >= m_tConfig.subGameRunPoint)
            {
                m_nSubGameRunPoint = 0;
                m_nSubGameCnt = random(5) + 1; // 1~5회 진행
                m_next_ms = ms + 100;
                m_eGameState = GAME_SUBGAME;
                break;
            }

            m_eGameState = GAME_END;
            m_next_ms = 0;
            break;

        case GAME_SUBGAME:
            if ((r = IsCommReady()) == false)
                break;

            if (ms < m_next_ms)
                break;

            // 0 ~ 10000원 (천원 단위)
            winScore = random(11) * 1000;

            // 이번 부가게임에서의 누적점수
            m_nSubGameBankScore += winScore;

            MsgSend(ASN_MSG_TYPE1_DELIVER,
                    ASN_MSG_TYPE2_D_SUBGAME_START,
                    0, 0, TRUE);
            Sleep(50); // 부가게임 진행 화면 표시
            MsgSend(ASN_MSG_TYPE1_DELIVER,
                    ASN_MSG_TYPE2_D_SUBGAME_WIN,
                    winScore, 0, TRUE);
            Sleep(10);
            MsgSend(ASN_MSG_TYPE1_DELIVER,
                    ASN_MSG_TYPE2_D_SUBGAME_BANK,
                    m_nSubGameBankScore, 0, TRUE);
            Sleep(50); // 당첨/누적점수  표시
            MsgSend(ASN_MSG_TYPE1_DELIVER,
                    ASN_MSG_TYPE2_D_SUBGAME_END,
                    0, 0, TRUE);

            Log(1, "부가게임(%d): 당첨=%u, 누적=%u",
                m_nSubGameCnt, winScore, m_nSubGameBankScore);

            if (--m_nSubGameCnt <= 0)
            {
                m_eGameState = GAME_END;
                m_next_ms = ms + 2000;
            }

            m_next_ms = ms + 1500;
            break;

        case GAME_END:
            if ((r = IsCommReady()) == false)
                break;
            if (ms < m_next_ms)
                break;

            // 게임 일시 정지가 발생하지 않도록 여기에서 게임 진행 시간을
            // 만족한 후에 누적점수와 게임 종료 메시지를 송신한다.
            // 게임 종료 후에 1초를 추가로 기다려야 하므로 여기에서 1초를 덜
            // 기다려도 되지만 autoGamePeriod 설정으로 조절가능하므로...
            if (ms - m_start_ms > m_tConfig.autoGamePeriod)
            {
                m_delay_req_ms = 0;

                // 메인게임의 누적점수에 부가게임에서 얻은 점수 적용
                m_lBankScore += m_nSubGameBankScore;
                m_nSubGameBankScore = 0;

                MsgSend(ASN_MSG_TYPE1_DELIVER, ASN_MSG_TYPE2_D_BANK_SCORE,
                        m_lBankScore, 0, TRUE);

                Log(1, "게임 종료");
                LinkSendPeriodSet(LINK_SEND_PERIOD_IDLE);
                MsgSend(ASN_MSG_TYPE1_DELIVER, ASN_MSG_TYPE2_D_GAME_END,
                        0, 0, TRUE);

                m_eGameState = GAME_REST;
                // 게임 일시 정지 기능 지원을 위해서 게임 종료 후 1초 이상
                // 기다린 후에 다음 게임을 진행할 수 있다.
                m_next_ms = ms + 1000;
            }
            break;

        case GAME_REST:
            if (m_bGameEndNow || ms >= m_next_ms)
            {
                // 게임 일시 정지 요청이 수신된 경우 일시 정지가 해제될 때까지
                // 더 기다린다.  여기에서는 요청된 시간 + 2초동안 해제되기를
                // 기다리며 OIDD에 의해 해제되지 않더라도 요청된 시간 이상
                // 기다렸으므로 다음 게임을 진행한다.
                if (m_delay_req_ms)
                {
                    m_next_ms = ms + m_delay_req_ms + 2000;
                    m_delay_req_ms = 0;
                }
                else
                {
                    m_eGameState = GAME_READY;
                }
            }
            r = false;
            break;
    }

    return r;
}

void CGame::RecvData(GameMsg_t *msg)
{
    inherited::RecvData(msg);

    AsnMsgHeader_t *msgHeader = &msg->header;
    uint32_t v1, d, t;

    switch (msgHeader->msgType1)
    {
        case ASN_MSG_TYPE1_CONNECT:
        case ASN_MSG_TYPE1_DISCONNECT:
        case ASN_MSG_TYPE1_DELIVER:
        case ASN_MSG_TYPE1_LINK:
            // ignore ACK message from OIDD
            break;

        case ASN_MSG_TYPE1_RECEIVE:
            v1 = msg->body.choice.gameData.value1;
            switch (msgHeader->msgType2)
            {
                case ASN_MSG_TYPE2_D_INPUT_AMT:
                    if (msg->header.actionId != m_prevInputAmtActionId)
                    {
                        m_prevInputAmtActionId = msg->header.actionId;
                        m_lInputMoney += (int)v1;
                        MsgSend(ASN_MSG_TYPE1_DELIVER, ASN_MSG_TYPE2_D_POT_AMT,
                                m_lInputMoney, 0, TRUE);
                    }
                    else
                    {
                        Log(1, "재전송된 투입 금액 무시 (%u)\n", v1);
                    }
                    break;

                case ASN_MSG_TYPE2_D_DELAY_REQ:
                    if (msg->header.actionId != m_prevDelayReqActionId)
                    {
                        m_prevDelayReqActionId = msg->header.actionId;
                        // 100msec 단위의 value1을 1msec 단위로 저장.
                        m_delay_req_ms = v1 * 100;
                        Log(1, "게임 일시 정지 요청 = %u (msecs)\n",
                            m_delay_req_ms);
                    }
                    else
                    {
                        Log(1, "재전송된 일시정지 무시 (%u)\n"), v1;
                    }
                    break;

                case ASN_MSG_TYPE2_D_DELAY_REL:
                    // delay req에 의해 GAME_REST 단계에서 기다리는 중인 경우
                    // 일시 정지 해제
                    if (m_eGameState == GAME_REST)
                        m_next_ms = 0;
                    break;

                case ASN_MSG_TYPE2_D_LINK_REQ:
                    break;

                case ASN_MSG_TYPE2_D_ERROR:
                    break;

                default:
                    Log(1, "OIDD->GAME: RECEIVE, %#x",
                        msgHeader->msgType2);
                    break;
            }
            break;

        case ASN_MSG_TYPE1_GET:
            break;
    }
}

void CGame::Redraw(int redraw)
{
    static int oldErr, cnt;

    if (m_eError != oldErr)
    {
        oldErr = m_eError;
        ++redraw;
    }

    redraw += IsNewLogExist();

    // 1초 동안 IDLE 상태였다면 혹시 놓쳤을 지 모르는 경우에 대비해서 redraw
    if (cnt > 50)
        ++redraw;

    if (redraw)
    {
        InvalidateRect(m_hWnd, NULL, false);
        cnt = 0;
    }
    else
    {
        ++cnt;
    }
}

void CGame::TimerTick(DWORD ms)
{
    inherited::TimerTick(ms);

    int redraw = ProcessGame(ms);
    Redraw(redraw);
}

void CGame::ReadConfig(void *readhandle)
{
    CHAR str[128];

    inherited::ReadConfig(readhandle);

    GetPrivateProfileStringA("OIDD", "AutoGamePeriod", "",
		(LPSTR)str, sizeof(str), (LPCSTR) readhandle);
    if (*str)
    {
        sscanf(str, "%ld", &m_tConfig.autoGamePeriod);
        if (m_tConfig.autoGamePeriod < N_AUTO_GAME_PERIOD_MIN)
            m_tConfig.autoGamePeriod = N_AUTO_GAME_PERIOD_MIN;
    }

    GetPrivateProfileStringA("OIDD", "SubGameRunPoint", "",
		(LPSTR)str, sizeof(str), (LPCSTR) readhandle);
    if (*str)
        sscanf(str, "%d", &m_tConfig.subGameRunPoint);
}

