#ifndef __MAIN_H_
#define __MAIN_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <windows.h>
#include "resource.h"
#include "game.h"

class CMain 
{
public:
	CMain();
	~CMain();

public:
	int Main(HINSTANCE hInstance, int nCmdShow);
	static HINSTANCE m_hInstance;
	
private:
	HWND m_hWnd;
#define MAX_LOADSTRING 100
	TCHAR m_szTitle[MAX_LOADSTRING];
	TCHAR m_szWindowClass[MAX_LOADSTRING];
	
	ATOM RegisterClass(HINSTANCE hInstance);
	bool InitInstance(HINSTANCE, int);
	static INT_PTR CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static void functionflash(HWND hX, const char* StrArg);

private:
	CGame m_tGameProc;
};
#endif 